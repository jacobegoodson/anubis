Class {
	#name : #SkiveCodeArea,
	#superclass : #SpRichTextPresenter,
	#category : #Anubis
}

{ #category : #initialization }
SkiveCodeArea class >> genArea [
	| sk |
	sk := SkiveCodeArea new. 
	sk textStyler: (SpNullTextStyler new).
	^sk.
]
